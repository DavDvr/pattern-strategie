<?php

namespace Strategy\App\Entity;
use Strategy\App\Interfaces\PersonneInterface;

/**
 * Voici ma classe Personne et l’interface qu’elle implémente.
 * La seule chose qu’elle fait est de retourner la chaîne de caractères 'Bonjour les cocos'
 * Class Personne
 * @package Strategy\App\Entity
 */
class Personne implements PersonneInterface
{
    public function donnerPhrase(): string
    {
       return 'Bonjour les cocos' ;
    }
}