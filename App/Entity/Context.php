<?php

namespace Strategy\App\Entity;
use Strategy\App\Interfaces\PersonneInterface;
use Strategy\App\Interfaces\StrategieInterface;

/**
 * Objet Contexte, partie intégrante du pattern Strategy.
 * Il gère simplement une référence à la stratégie,
 * à laquelle il transmet les requêtes des clients via la délégation à la méthode reagir.
 * C’est en quelque sorte le liant entre le code client et nos différentes stratégies concrètes.
 * Class Context
 * @package Strategy\App\Entity
 */
class Context
{
    private StrategieInterface $strategy;

    public function __construct(StrategieInterface $strategy)
    {
        $this->strategy = $strategy;
    }

    public function exprimeReaction(PersonneInterface $personne): string
    {
        return $this->strategy->reagir($personne);
    }
}