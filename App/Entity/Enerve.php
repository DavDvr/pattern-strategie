<?php

namespace Strategy\App\Entity;
use Strategy\App\Interfaces\PersonneInterface;
use Strategy\App\Interfaces\StrategieInterface;

/**
 * Cette classe implémente StrategieInterface et donc sa methode reagir()
 * qui prend en paramètre PersonneInterface
 * Class Enerve
 */
class Enerve implements StrategieInterface
{

    public function reagir(PersonneInterface $personne): string
    {
       return strtoupper($personne->donnerPhrase(). ' !!!')."<br>";
    }
}
