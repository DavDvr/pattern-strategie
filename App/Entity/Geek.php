<?php

namespace Strategy\App\Entity;

use Strategy\App\Interfaces\PersonneInterface;
use Strategy\App\Interfaces\StrategieInterface;



/**
 * Cette classe implémente StrategieInterface et donc sa methode reagir()
 * qui prend en paramètre PersonneInterface
 * Class Enerve
 */
class Geek implements StrategieInterface
{

    public function reagir(PersonneInterface $personne): string
    {
       return str_replace("o", "0", $personne->donnerPhrase())."<br>";
    }
}