<?php

namespace Strategy\App\Interfaces;

interface PersonneInterface
{
    public function donnerPhrase(): string;
}