<?php

namespace Strategy\App\Interfaces;

interface StrategieInterface
{
    public function reagir(PersonneInterface $personne): string;
}