<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Strategy</title>
</head>
<body>
<?php

use Strategy\App\Entity\Context;
use Strategy\App\Entity\Enerve;
use Strategy\App\Entity\Geek;
use Strategy\App\Entity\Jovial;
use Strategy\App\Entity\Personne;
require(dirname(__DIR__) . "/vendor/autoload.php");


echo "__________________________________Strategy Jovial <br>";
    $personJovial = new Personne();
    $contextJovial = new Context(new Jovial());
    echo $contextJovial->exprimeReaction($personJovial);

echo "__________________________________Strategy Enerve<br>";
    $personEnerve = new Personne();
    $contextEnerve = new Context(new Enerve());
    echo $contextEnerve->exprimeReaction($personEnerve);

echo "__________________________________Strategy Geek<br>";

    $personGeek = new Personne();
    $contextGeek = new Context(new Geek());
    echo $contextGeek->exprimeReaction($personGeek);
?>

</body>
</html>
